import csv
from csv import DictReader, DictWriter

with open(f"SraRunTable.txt") as f_in:
    SRA = list(DictReader(f_in))

TARGET_SRA_FASTQ = expand("from-sra/{srr}_{rp}.fastq.gz", srr=[row["Run"] for row in SRA], rp=[1, 2])
TARGET_OUTPUT = expand("output/{sheet}.csv", sheet=["samples", "primers", "oligos"])

rule all:
    input: TARGET_OUTPUT + TARGET_SRA_FASTQ

def parse_sample_name(txt):
    gene = tpoint = rep = env = ""
    parts = txt.split(".")
    if len(parts) == 3:
        match = re.match("[tT]([0-9]+)", parts[1])
        if match:
            # regular fitness sample, later timepoint, with replicates
            gene, tpoint, rep = parts
            tpoint = match.group(1)
        else:
            match = re.match("[tT]([0-9]+)", parts[2])
            if match:
                # environment "MULTI" sample
                gene, env, tpoint = parts
                # is that "T0" a timepoint though?  Not sure.
                tpoint = ""
            else:
                # environment sample
                gene, env, rep = parts
    else:
        # Fitness T0, no replicate
        match = re.match("[tT]([0-9]+)", parts[1])
        gene, tpoint = parts
        tpoint = match.group(1)
    out = {
        "Gene": gene,
        "Timepoint": tpoint,
        "Replicate": rep,
        "Environment": env}
    return out

rule all_output:
    input: TARGET_OUTPUT

# special case versus the others below
rule setup_oligos_sheet:
    output: "output/oligos.csv"
    input: "from-paper/supp_data_5_02_synth_mut_oligos.csv"
    run:
        with open(input[0]) as f_in, open(output[0], "w") as f_out:
            writer = DictWriter(
                f_out, ["Gene", "Upstream", "Target", "Downstream"], lineterminator="\n")
            writer.writeheader()
            for row in csv.reader(f_in):
                upstream, target, downstream = re.match(
                    r"([^()]+)\(([^()]+)\)([^()]+)", row[1]).groups()
                writer.writerow({
                    "Gene": row[0],
                    "Upstream": upstream,
                    "Target": target,
                    "Downstream": downstream})

SUPP_DATA_5 = [
    "01_del_wt",
    # (Compare the sequences outside the parentheses in this second sheet with
    # the primer sequences in the third sheet; in terms of subsequent
    # amplification primers the third sheet is all that's needed, but the
    # second shows what's between for WT)
    #"02_synth_mut_oligos",
    "03_amp_mut_oligos",
    "04_ypd_dfe_seq_primers",
    "05_gene_rt_primers",
    "06_exp_seq_primers",
    "07_multi_env_seq_primers"]

rule setup_primers_sheet:
    output: "output/primers.csv"
    input: expand("from-paper/supp_data_5_{sheet}.csv", sheet=SUPP_DATA_5)
    params:
        sheet=SUPP_DATA_5
    run:
        with open(output[0], "w") as f_out:
            writer = DictWriter(
                f_out, ["Category", "Gene", "Direction", "SeqID", "Seq"], lineterminator="\n")
            writer.writeheader()
            for sheet, path in zip(params.sheet, input):
                with open(path) as f_in:
                    patterns = {
                        "01_del_wt": r"([A-Z0-9]+) .* ([FR])",
                        "03_amp_mut_oligos": r"([A-Z0-9]+) .* ([FR])",
                        "04_ypd_dfe_seq_primers": r"([A-Z0-9]+) +([FR]).*",
                        "05_gene_rt_primers": r"([A-Z0-9]+)_RT_(R)",
                        "06_exp_seq_primers": r"([A-Z0-9]+)[ -].*(Reverse)?.*",
                        "07_multi_env_seq_primers": r"([A-Z0-9]+) ([FR]).*"}
                    for row in csv.reader(f_in):
                        match = re.match(patterns[sheet], row[0])
                        if not match:
                            raise ValueError(sheet + ' ' + row[0])
                        gene, direction = match.groups()
                        if sheet == "06_exp_seq_primers":
                            direction = "R" if direction == "Reverse" else "F"
                        writer.writerow({
                            "Category": sheet,
                            "Gene": gene,
                            "Direction": direction,
                            "SeqID": row[0],
                            "Seq": row[1]})

rule setup_samples_sheet:
    output: "output/samples.csv"
    input: "SraRunTable.txt"
    run:
        fieldnames = [
            "Sample", "Category", "Gene", "Timepoint", "Replicate", "Environment",
            "SRARun", "SRAInstrument", "SRALibrarySource"]
        rows_out = []
        with open(input[0]) as f_in, open(output[0], "w") as f_out:
            writer = DictWriter(f_out, fieldnames=fieldnames, lineterminator="\n")
            writer.writeheader()
            for row in DictReader(f_in):
                names = {row[thing] for thing in ("Sample Name", "Library Name", "Isolate")}
                if len(names) > 1:
                    raise ValueError
                row_out = parse_sample_name(row["Sample Name"])
                if row_out["Environment"] in ("DNA", "RNA"):
                    cat = "Expression"
                elif row_out["Environment"]:
                    cat = "Environment"
                else:
                    cat = "Fitness"
                row_out.update({
                    "Sample": row["Sample Name"],
                    "Category": cat,
                    "SRARun": row["Run"],
                    "SRAInstrument": row["Instrument"],
                    "SRALibrarySource": row["LibrarySource"]})
                rows_out.append(row_out)
            rows_out = sorted(
                rows_out,
                key=lambda r: (r["Category"], r["Timepoint"], r["Environment"], r["Gene"], r["Replicate"]))
            writer.writerows(rows_out)

rule all_sra_fastq:
    input: TARGET_SRA_FASTQ

rule sra_fastq:
    output: expand("from-sra/{{srr}}_{rp}.fastq.gz", rp=[1, 2])
    shell: "fastq-dump --split-files --gzip {wildcards.srr} --outdir from-sra"
