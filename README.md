# Data Gathering from Shen 2022

Aggregating metadata and sequences from:

Shen, X., Song, S., Li, C. et al.
Synonymous mutations in representative yeast genes are mostly strongly non-neutral.
Nature 606, 725–731 (2022).
<https://doi.org/10.1038/s41586-022-04823-w>

Direct readable paper link: <https://rdcu.be/da2Wh>

 * NCBI BioProject for sequencing of the mutant strains:
   [PRJNA750109](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA750109)
 * Code (two locations):
   * <https://github.com/song88180/Mutational-Fitness-Effects>
   * <https://doi.org/10.5281/zenodo.5908478>
